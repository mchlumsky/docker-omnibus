Omnibus Dockerfiles
===================

Dockerfiles for building docker images with omnibus installed.

The docker images are "executable". They take a git repository containing your omnibus project tree as a parameter and build the omnibus package.

Requirements
------------
Any OS with Docker installed.

Docker image build instructions
-------------------------------

 1. git clone https://github.com/mchlumsky/docker-omnibus.git
 1. cd docker-omnibus
 1. (For EL5) `docker build --rm -f Dockerfile.el5 -t omnibus/el5`
 1. (For EL6) `docker build --rm -f Dockerfile.el6 -t omnibus/el6`
 1. (For EL7) `docker build --rm -f Dockerfile.el7 -t omnibus/el7`

How to use the image to build the omnibus package
-------------------------------------------------

```bash
docker run -v <destination directory for packages>:/pkg <docker image> <git url of your omnibus project> <git commit/branch/tag> <omnibus project name>
```

Example:
```bash
docker run -v /tmp/pkg/:/pkg omnibus/el7 https://github.com/mchlumsky/omnibus-example.git master example
```
