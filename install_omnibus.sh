#!/bin/bash
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
curl -sSL https://get.rvm.io | bash -s stable

source "/usr/local/rvm/scripts/rvm"

RUBY_VERSION=2.2.3
rvm install $RUBY_VERSION
yum clean all
rvm use --default $RUBY_VERSION
gem install --no-rdoc --no-ri omnibus
